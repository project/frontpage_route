<?php

namespace Drupal\frontpage_route\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * A simple empty response.
 *
 * This allows you to construct the front page with blocks.
 */
class FrontPageController extends ControllerBase {

  public function frontPage() {
    return ['#markup' => ''];
  }

}
